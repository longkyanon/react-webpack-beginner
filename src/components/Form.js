import React, {Component} from "react";
import ReactDOM from "react-dom";

import { DatePicker } from 'antd';

class Form extends Component {
    constructor() {
        super();

        this.state = {
            value: ""
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const {value} = event.target;
        this.setState(() => {
            return {
                value
            };
        });
    }

    render() {
        return (
            <div>
                <DatePicker />
            </div>
        );
    }
}

export default Form;